import os

def askName():
        res = input('Name of the stack:')
        # Validate res
        return res

def askUseTraefik():
        res = input('Use Traefik? 1 for yes, 0 for no: ')
        # Validate res
        return bool(int(res))

def createDirectory(dir):
        os.mkdir(dir)

def createStartScript(dir, name):
        f = open((dir + '/start.sh'), 'w')
        f.write("sudo -E bash -c 'docker stack deploy --compose-file=" + name  +                                                                                                                                                              "-stack.yml " + name  + "'")
        writeNewLine(f)
        f.close()

def writeNewLine(f):
        f.write('\n')

def writeTab(f):
        f.write('  ')

def createStackFile(dir, name, useTraefik):
        f = open((dir + '/' + name + '-stack.yml'), 'w')
        f.write("version: '3.3'")
        writeNewLine(f)
        writeNewLine(f)
        f.write('services:')
        writeNewLine(f)
        writeTab(f)
        f.write(name + ':')
        writeNewLine(f)
        writeTab(f)
        writeTab(f)
        f.write('image:')
        writeNewLine(f)
        writeTab(f)
        writeTab(f)
        f.write('deploy:')
        writeNewLine(f)
        writeTab(f)
        writeTab(f)
        writeTab(f)
        f.write('mode: replicated')
        writeNewLine(f)
        writeTab(f)
        writeTab(f)
        writeTab(f)
        f.write('replicas: 1')
        writeNewLine(f)

        if (useTraefik):
                writeTab(f)
                writeTab(f)
                writeTab(f)
                f.write('labels:')
                writeNewLine(f)

                writeTab(f)
                writeTab(f)
                writeTab(f)
                writeTab(f)
                f.write("- 'traefik.enable=true'")
                writeNewLine(f)

                writeTab(f)
                writeTab(f)
                writeTab(f)
                writeTab(f)
                f.write("- 'traefik.http.routers.x.rule=Host(``)'")
                writeNewLine(f)

                writeTab(f)
                writeTab(f)
                writeTab(f)
                writeTab(f)
                f.write("- 'traefik.http.routers.x.entrypoints=web'")
                writeNewLine(f)

                writeTab(f)
                writeTab(f)
                writeTab(f)
                writeTab(f)
                f.write("- 'traefik.http.services.x.loadbalancer.server.port=0'"                                                                                                                                                             )
                writeNewLine(f)

                writeTab(f)
                writeTab(f)
                writeTab(f)
                writeTab(f)
                f.write("- 'traefik.http.routers.x-secured.rule=Host(``)'")
                writeNewLine(f)

                writeTab(f)
                writeTab(f)
                writeTab(f)
                writeTab(f)
                f.write("- 'traefik.http.routers.x-secured.entrypoints=web-secur                                                                                                                                                             ed'")
                writeNewLine(f)

                writeTab(f)
                writeTab(f)
                writeTab(f)
                writeTab(f)
                f.write("- 'traefik.http.routers.x-secured.tls.certresolver=mytl                                                                                                                                                             schallenge'")
                writeNewLine(f)

        if (useTraefik):
                writeNewLine(f)
                f.write('networks:')
                writeNewLine(f)
                writeTab(f)
                f.write('traefik:')
                writeNewLine(f)
                writeTab(f)
                writeTab(f)
                f.write('driver: overlay')
                writeNewLine(f)
                writeTab(f)
                writeTab(f)
                f.write('external: true')
                writeNewLine(f)

        f.close()

def createReadme(dir, name):
        f = open((dir + '/README.md'), 'w')
        f.write('# ' + name)
        f.close()

def createSkeleton(dir, name, useTraefik):
        createDirectory(name)
        createStartScript(dir, name)
        createStackFile(dir, name, useTraefik)
        createReadme(dir, name)

def createStack():
        name = askName()
        useTraefik = askUseTraefik()
        createSkeleton(name, name, useTraefik)


createStack()
