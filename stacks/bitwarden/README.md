# Bitwarden

| <!-- --> | <!-- --> |
|--|--|
| **Image** | [**vaultwarden/server**](https://hub.docker.com/r/vaultwarden/server) |
| **Port(s)** |  **8060** |
| **Volumes** | **./bitwarden/data**, |
| **FQDN** | **https://bitwarden.lan** |

## Run Bitwarden
call the start script:
```bash
./start.sh
```
