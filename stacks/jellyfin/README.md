# Jellyfin

| <!-- --> | <!-- --> |
|--|--|
| **Image** | [**ghcr.io/linuxserver/jellyfin**](https://hub.docker.com/r/linuxserver/jellyfin) |
| **Port(s)** |  **8096** |
| **Volumes** | **./jellyfin/config**, |
| **FQDN** | **https://jellyfin.lan** |

## Run Jellyfin
call the start script:
```bash
./start.sh
```
