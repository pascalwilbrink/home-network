# Guacamole

| <!-- --> | <!-- --> |
|--|--|
| **Image** | [**oznu/guacamole**](https://hub.docker.com/r/oznu/guacamole) |
| **Port(s)** |  **8070** |
| **Volumes** | **./guacamole/config**, |
| **FQDN** | **https://guacamole.lan** |

## Run Guacamole
call the start script:
```bash
./start.sh
```
