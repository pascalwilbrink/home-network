# Heimdall

| <!-- --> | <!-- --> |
|--|--|
| **Image** | [**ghcr.io/linuxserver/heimdall**](https://hub.docker.com/r/linuxserver/heimdall) |
| **Port(s)** |  **8080** |
| **Volumes** | **./heimdall/config**, |
| **FQDN** | **https://heimdall.lan** |

## Run Heimdall
call the start script:
```bash
./start.sh
```
