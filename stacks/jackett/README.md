# Jackett

| <!-- --> | <!-- --> |
|--|--|
| **Image** | [**ghcr.io/linuxserver/jackett**](https://hub.docker.com/r/linuxserver/jackett) |
| **Port(s)** |  **9117** |
| **Volumes** | **./jackett/config**, |
| **FQDN** | **https://jackett.lan** |

## Run Jackett
call the start script:
```bash
./start.sh
```
