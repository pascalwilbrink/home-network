# Deluge

| <!-- --> | <!-- --> |
|--|--|
| **Image** | [**linuxserver/deluge**](https://hub.docker.com/r/linuxserver/deluge) |
| **Port(s)** |  **8112** |
| **Volumes** | **./deluge/config**, |
| **FQDN** | **https://deluge.lan** |

## Run Deluge
call the start script:
```bash
./start.sh
```
