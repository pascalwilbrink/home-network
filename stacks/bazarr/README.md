# Bazarr

| <!-- --> | <!-- --> |
|--|--|
| **Image** | [**linuxserver/bazarr**](https://hub.docker.com/r/linuxserver/bazarr) |
| **Port(s)** |  **6767** |
| **Volumes** | **./bazar/config**, |
| **FQDN** | **https://bazarr.lan** |

## Run Bazarr
call the start script:
```bash
./start.sh
```
