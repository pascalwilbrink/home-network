# Portainer

| <!-- --> | <!-- --> |
|--|--|
| **Image** | [**portainer/agent**](https://hub.docker.com/r/linuxserver/jellyfin)**,**[**portainer/portainer-ce]() |
| **Port(s)** |  **8096** |
| **Volumes** | **./jellyfin/config**, |
| **FQDN** | **https://portainer.lan** |

## Run Portainer
call the start script:
```bash
./start.sh
```
