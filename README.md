# Home Network

## TOC
  - [Stacks](#stacks)
  - [Overview](#overview)
  - [Installation](#installation)
  - [Setup new node](#setup-new-node)
  - [Improvements](#improvements)
  
## Stacks
* [Bazarr](stacks/bazarr/README.md)
* [Bitwarden](stacks/bitwarden/README.md)
* [Deluge](stacks/deluge/README.md)
* [Fluentbit](stacks/fluentbit/README.md)
* [Guacamole](stacks/guacamole/README.md)
* [Heimdall](stacks/heimdall/README.md)
* [Jackett](stacks/jackett/README.md)
* [Jellyfin](stacks/jellyfin/README.md)
* [Portainer](stacks/portainer/README.md)
* [Radarr](stacks/radarr/README.md)
* [Requestrr](stacks/requestrr/README.md)
* [Sonarr](stacks/sonarr/README.md)
* [Traefik](stacks/traefik/README.md)
* [Wikijs](stacks/wikijs/README.md)

## Overview

## Installation
Run the [Setup new node](#setup-new-node) for each Node in your stack.

## Setup new node
In order for a node to work on the stack, some prerequisites are required.
After these are installed and configured, the configuration step can take place.

### Prerequisites
* [Docker](#install-docker)
* [Docker Compose](#install-docker-compose)
* [Glances(optional)](#install-glances)

##### Install Docker

1) Update and upgrade
```bash 
sudo apt-get update && sudo apt-get upgrade
```

2) Download get-docker script

```bash 
curl -fsSL https://get.docker.com -o get-docker.sh
```

Execute command:
```bash 
sudo sh get-docker.sh
```

3) Add user to the Docker Group
```bash 
sudo usermod -aG docker [username]
```

example:
```bash 
sudo usermod -aG docker ubuntu
```

#### Install Docker Compose

1) Install dependencies
```bash 
sudo apt-get install libffi-dev libssl-dev
sudo apt-get install python3-dev
sudo apt-get install -y python3 python3-pip
```

2) Install Docker-Compose
```bash
sudo pip3 install docker-compose
```

2) Start containers on boot
```bash
sudo systemctl enable docker
```

#### Install Glances
-- TBD ---

### Configuration
1) Change the hostname
```bash
sudo nano /etc/hostname
```

2) Change the hostsfile
```bash
sudo nano /etc/hosts
```

3) Create keypair
```bash
ssh-keygen
```

4) Copy public key
```bash
cat ~/.ssh/id_rsa.pub
```

or
```bash
ssh-copy-id [user]@[ip]
```

example:
```bash
ssh-copy-id ubuntu@192.168.10.2
```

Note: copy the ssh-key to all the existing nodes in the stack

## Improvements
* Create Ansible scripts
