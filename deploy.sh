#! /bin/bash

services=("radarr" "sonarr" "deluge" "bitwarden" "portainer" "traefik" "guacamole" "bazarr" "heimdall" "jackett" "jellyfin" "requestrr" "wikijs")

case "${services[@]}" in *$1*) 
  echo "(Re)starting " $1
  cd ./stacks/$1 && sh ./start.sh
esac


